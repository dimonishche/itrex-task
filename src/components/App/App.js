import React from 'react'
import {Link, Route, Switch, Redirect} from 'react-router-dom'
import {Navbar, Nav, NavItem} from 'react-bootstrap'
import {LinkContainer} from 'react-router-bootstrap'
import Register from '../../containers/Register'
import Login from '../../containers/Login'
import Dashboard from '../../containers/Dashboard'
import Profile from '../../containers/Profile'
import './App.scss'

const App = ({isLogged, logoutUser}) => {
  return (
    <div>
      <Navbar collapseOnSelect>
        <Navbar.Header>
          <Navbar.Brand>
            <LinkContainer to='/'><a>ITRex Task</a></LinkContainer>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          {isLogged ? (
            <Nav>
              <LinkContainer to='/dashboard'><NavItem eventKey={1}>Dashboard</NavItem></LinkContainer>
              <LinkContainer to='/profile'><NavItem eventKey={2}>Profile</NavItem></LinkContainer>
            </Nav>
          ) : (
            <Nav>
              <LinkContainer to='/login'><NavItem eventKey={3}>Login</NavItem></LinkContainer>
              <LinkContainer to='/register'><NavItem eventKey={4}>Register</NavItem></LinkContainer>
            </Nav>
          )}
          {isLogged ? (
            <Nav pullRight>
              <NavItem eventKey={5} onClick={logoutUser}>LogOut</NavItem>
            </Nav>
          ) : null}
        </Navbar.Collapse>
      </Navbar>

      <Route exact path='/' render={props => (isLogged ? <Dashboard /> : <Redirect to='/login'/>)}/>
      <Route path='/dashboard' render={props => (isLogged ? <Dashboard /> : <Redirect to='/login'/>)}/>
      <Route path='/profile' render={props => (isLogged ? <Profile /> : <Redirect to='/login'/>)}/>
      <Route path='/login' render={props => (!isLogged ? <Login /> : <Redirect to='/'/>)}/>
      <Route path='/register' render={props => (!isLogged ? <Register {...props}/> : <Redirect to='/'/>)}/>

    </div>
  )
}

export default App