import React from 'react'
import {PageHeader, Grid} from 'react-bootstrap'
import './Profile.scss'

const Profile = ({userInfo, isFetching, error}) => {
  return (
    <Grid>
      <PageHeader>Profile</PageHeader>
      {isFetching ?
        (<p className='loading'>Loading...</p>) :
        (!!error ? <p className='loadingError'>{error}</p> : (
          <ul className='profileData'>
            <li>Name: {userInfo.name}</li>
            <li>Email: {userInfo.email}</li>
            <li>Company: {userInfo.company}</li>
          </ul>
        ))}
    </Grid>
  )
}

export default Profile