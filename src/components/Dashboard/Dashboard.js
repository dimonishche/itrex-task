import React from 'react'
import InfiniteScroll from 'react-infinite-scroller'
import {PageHeader, Grid} from 'react-bootstrap'
import {parseDate} from '../../utils'
import './Dashboard.scss'

const Dashboard = ({usersData, endOfUsersList, getUsers, page, usersAmount, currentUserId}) => {
  return (
    <Grid>
      <PageHeader>Dashboard</PageHeader>
      <p className='usersAmount'>Users amount: {usersAmount}</p>
      <InfiniteScroll
        pageStart={page}
        loadMore={getUsers}
        hasMore={!endOfUsersList}
        loader={<h4>Loading...</h4>}
        threshold={20}
        useWindow={true}
        className='usersList'
      >
        {usersData.map(item =>
          <div className='userInfo' key={item.id}>
            <p>Name: {item.name}</p>
            <p>Email: {item.email}</p>
            <p>Company: {item.company}</p>
            <p>Status: {+currentUserId === +item.id ? 'online' : 'Were online at ' + parseDate(item.lastTime)}</p>
          </div>
        )}
      </InfiniteScroll>
    </Grid>
  )
}

export default Dashboard