import React from 'react'
import './Register.css'
import {Field, reduxForm} from 'redux-form'
import {PageHeader, Grid, Row, Col, Button} from 'react-bootstrap'

const validate = values => {
  const errors = {}
  if (!values.email)
    errors.email = 'The field is required'
  else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email))
    errors.email = 'Invalid email address'

  if (!values.password)
    errors.password = 'The field is required'
  else if (values.password.length < 3 || values.password.length > 24)
    errors.password = 'Password length should be from 3 to 24 symbols.'
  else if (values.password.toLowerCase() === values.password)
    errors.password = 'Password should contain uppercase letters.'
  if (!values.passwordConfirm)
    errors.passwordConfirm = 'The field is required'
  else if (values.passwordConfirm !== values.password)
    errors.passwordConfirm = 'Password is not the same as password field'
  if (!values.name)
    errors.name = 'The field is required'
  else if (values.name.length < 3 || values.name.length > 100)
    errors.name = 'Name length should be from 3 to 100 symbols.'
  if (!values.company)
    errors.company = 'The field is required'

  return errors
}

const renderField = ({input, label, type, className, id, meta: {touched, error, warning}}) => (
  <div className='field'>
    <input {...input}
      placeholder={label}
      type={type}
      className={`${className} ${touched && error ? 'errorField' : ''}`}
      id={id}
    />
    {touched &&
    error ?
      <span className='inputError'>
          {error}
        </span> : <span className='inputError'></span>}
  </div>
)

const Register = ({handleSubmit}) => {
  return (
    <Grid className='registration'>
      <PageHeader>Registration</PageHeader>
      <Row>
        <Col lgOffset={3} lg={6} mdOffset={2} md={8}>
          <form onSubmit={handleSubmit}>
            <label htmlFor='email'>Email</label>
            <Field name='email' id='email' type='text' className='inputStyle form-control' component={renderField}
                   label='Email'/>
            <label htmlFor='password'>Password</label>
            <Field name='password' id='password' type='password' className='inputStyle form-control'
                   component={renderField} label='Password'/>
            <label htmlFor='passwordConfirm'>Confirm password</label>
            <Field name='passwordConfirm' id='passwordConfirm' type='password' className='inputStyle form-control'
                   component={renderField} label='Confirm password'/>
            <label htmlFor='name'>Name</label>
            <Field name='name' id='name' type='text' className='inputStyle form-control' component={renderField}
                   label='name'/>
            <label htmlFor='company'>Company</label>
            <Field name='company' id='company' type='text' className='inputStyle form-control' component={renderField}
                   label='Company'/>
            <Button className='register' block type='submit'>REGISTER</Button>
          </form>
        </Col>
      </Row>
    </Grid>
  )
}

export default reduxForm({form: 'registerForm', validate})(Register)