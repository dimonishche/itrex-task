import React from 'react'
import {Field, reduxForm} from 'redux-form'
import {PageHeader, Grid, Row, Col, Button} from 'react-bootstrap'
import './Login.scss'

const validate = values => {
  const errors = {}
  if (!values.email)
    errors.email = 'The field is required'
  else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email))
    errors.email = 'Invalid email address'

  if (!values.password)
    errors.password = 'The field is required'

  return errors
}

const renderField = ({input, label, type, className, id, meta: {touched, error, warning}}) => (
  <div className='field'>
    <input {...input}
      placeholder={label}
      type={type}
      className={`${className} ${touched && error ? 'errorField' : ''}`}
      id={id}
    />
    {touched &&
    error ?
      <span className='inputError'>
          {error}
        </span> : <span className='inputError'></span>}
  </div>
)



const Login = (props) => {
  const {handleSubmit, loginError} = props
  return (
    <Grid>
      <PageHeader>Login</PageHeader>
      <Row>
        <Col lgOffset={3} lg={6} mdOffset={2} md={8}>
      <form onSubmit={handleSubmit}>
        <label className='formLabel' htmlFor='email'>Email</label>
        <Field name='email' id='email' type='text' className='inputStyle form-control error' component={renderField} label='Email'/>
        <label className='formLabel' htmlFor='password'>Password</label>
        <Field name='password' id='email' type='password' className='inputStyle form-control' component={renderField} label='Password'/>
        <Button block className='buttonStyle' type='submit'>LOG IN</Button>
        <span className='formError'>{loginError}</span>
      </form>
          </Col>
        </Row>
    </Grid>
  )
}

export default reduxForm({
  form: 'loginForm',
  validate
})(Login)

