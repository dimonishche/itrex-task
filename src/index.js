import React from 'react'
import ReactDOM from 'react-dom'
import {Provider} from 'react-redux'
import configureStore from './store/configureStore'
import App from './containers/App'
import {checkIsLogged} from './actions/global'
import {BrowserRouter as Router, Route} from 'react-router-dom'
import './index.scss'

const store = configureStore()

store.dispatch(checkIsLogged())

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <Route path="/" component={App}/>
    </Router>
  </Provider>,
  document.getElementById('root'));