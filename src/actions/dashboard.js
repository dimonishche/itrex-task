import createReducer from '../createReducer'
import {getUsers, getAmountOfUsers} from '../utils'

const SUCCESS_RECEIVED_USERS='SUCCESS_RECEIVED_USERS'
const END_OF_THE_USERS_LIST='END_OF_THE_USERS_LIST'
const GET_TOTAL_AMOUNT_OF_USERS='GET_TOTAL_AMOUNT_OF_USERS'
const INCREMENT_PAGE='INCREMENT_PAGE'

export const getUsersList = (page) => (dispatch, getState) => {
  const users = getUsers(page - 1, 10)
  if (!users.length) {
    dispatch({type: END_OF_THE_USERS_LIST})
    return false
  }

  dispatch({
    type: SUCCESS_RECEIVED_USERS,
    users
  })

  dispatch({type: INCREMENT_PAGE, nextPage: page+1})
}

export const getTotalAmountOfUsers = () => dispatch => {
  const usersAmount = getAmountOfUsers()
  dispatch({
    type: GET_TOTAL_AMOUNT_OF_USERS,
    usersAmount
  })
}

const initialState = {
  usersData: [],
  endOfUsersList: false,
  page: 0,
  usersAmount: 0
}

export const dashboard = createReducer(initialState, {
  [SUCCESS_RECEIVED_USERS]: ({usersData}, {users}) => ({usersData: [...usersData, ...users]}),
  [END_OF_THE_USERS_LIST]: (state, action) => ({endOfUsersList: true}),
  [INCREMENT_PAGE]: ({page}, {nextPage}) => ({page: nextPage}),
  [GET_TOTAL_AMOUNT_OF_USERS]: (state, {usersAmount}) => ({usersAmount})
})
