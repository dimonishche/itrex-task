import {getUserId, isCorrectLoginAndPassword, setUserId} from '../utils'
import createReducer from '../createReducer'

const SUCCESS_LOGGED = 'SUCCESS_LOGGED'
const SET_LOGIN_ERROR = 'SET_LOGIN_ERROR'
const LOGOUT_USER = 'LOGOUT_USER'

export const checkIsLogged = () => dispatch => {
  const userLoggedId = getUserId()
  if (!!userLoggedId) {
    dispatch({
      type: SUCCESS_LOGGED,
      userLoggedId})
    return true
  }

  dispatch(logoutUser)
}

export const loginUser = ({email, password}) => dispatch => {
  const loggedUser = isCorrectLoginAndPassword(email, password)
  if (!!loggedUser) {
    dispatch({
      type: SUCCESS_LOGGED,
      userLoggedId: loggedUser.id})
    dispatch(setLoginError)
    setUserId(loggedUser.id)
    return true
  }

  dispatch(setLoginError('Wrong email or password.'))
  return false
}

export const logoutUser = () => dispatch => {
  setUserId()
  dispatch({type: LOGOUT_USER})
}

export const setLoginError = (loginError = '') => ({
  type: SET_LOGIN_ERROR,
  loginError
})

const initialState = {
  userLoggedId: false,
  loginError: null
}

export const global = createReducer(initialState, {
  [SUCCESS_LOGGED]: (state, {userLoggedId}) => ({userLoggedId}),
  [LOGOUT_USER]: (state, action) => ({userLoggedId: false}),
  [SET_LOGIN_ERROR]: (state, {loginError}) => ({loginError})
})