import {getUserById} from '../utils'
import createReducer from '../createReducer'

const REQUEST_USER_INFO='REQUEST_USER_INFO'
const SUCCESS_RECEIVE_USER_DATA='SUCCESS_RECEIVE_USER_DATA'
const FAILED_RECEIVE_USER_DATA='FAILED_RECEIVE_USER_DATA'

export const getUserInfoById = (id) => dispatch => {
  const userInfo = getUserById(id)
  if (!userInfo) {
    dispatch({type: FAILED_RECEIVE_USER_DATA})
    return false
  }
  
  dispatch({
    type: SUCCESS_RECEIVE_USER_DATA,
    userInfo
  })
}

export const getCurrentUserInfo = () => (dispatch, getState) => {
  dispatch({type: REQUEST_USER_INFO})
  const currentId = getState().global.userLoggedId

  //fetch imitation
  setTimeout(() => dispatch(getUserInfoById(currentId)), 500)
}

const initialState = {
  userInfo: {},
  isFetching: true,
  error: null
}

export const profile = createReducer(initialState, {
  [REQUEST_USER_INFO]: (state, {isFetching}) => ({isFetching: true, error: null, userInfo: false}),
  [SUCCESS_RECEIVE_USER_DATA]: (state, {userInfo}) => ({userInfo, isFetching: false}),
  [FAILED_RECEIVE_USER_DATA]: (state, action) => ({isFetching: false, error: 'Error! Data not found!'})
})