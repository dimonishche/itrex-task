import {global} from '../actions/global'
import {profile} from '../actions/profile'
import {dashboard} from '../actions/dashboard'
import {combineReducers} from 'redux'
import {reducer as formReducer } from 'redux-form'

const rootReducer = combineReducers({
  global,
  profile,
  dashboard,
  form: formReducer
})

export default rootReducer