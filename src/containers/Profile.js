import React from 'react'
import Profile from '../components/Profile'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {getCurrentUserInfo} from '../actions/profile'

class ProfileContainer extends React.Component {
  componentWillMount() {
    this.props.getCurrentUserInfo()
  }
  
  render() {
    return <Profile {...this.props}/>
  }
}

const mapStateToProps = ({profile}) => ({
  userInfo: profile.userInfo,
  isFetching: profile.isFetching,
  error: profile.error
})

const mapDispatchToProps = dispatch => bindActionCreators({getCurrentUserInfo}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(ProfileContainer)