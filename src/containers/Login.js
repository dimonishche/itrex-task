import Login from '../components/Login'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {loginUser, setLoginError} from '../actions/global'


const mapStateToProps = ({global}) => ({
  loginError: global.loginError
})

const mapDispatchToProps = dispatch => bindActionCreators({onSubmit: loginUser, setLoginError}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Login)