import App from '../components/App'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {logoutUser} from '../actions/global'


const mapStateToProps = ({global}) => ({
  isLogged: !!global.userLoggedId
})

const mapDispatchToProps = dispatch => bindActionCreators({logoutUser}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(App)