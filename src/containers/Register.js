import React from 'react'
import Register from '../components/Register'
import { SubmissionError } from 'redux-form'
import {isEmailExist} from '../utils'

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

class RegisterContainer extends React.Component {
  submitRegister = (values) => {
    return sleep(1000) // simulate server latency
      .then(() => {
        if (isEmailExist(values.email)) {
          throw new SubmissionError({ email: 'User with this email is exist.'})
        } else {
          this.props.history.push('/login')
        }
      })
  }

  render () {
   return <Register onSubmit={this.submitRegister}/>
  }
}

export default RegisterContainer