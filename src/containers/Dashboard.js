import React from 'react'
import Dashboard from '../components/Dashboard'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {getUsersList, getTotalAmountOfUsers} from '../actions/dashboard'

class DashboardContainer extends React.Component {
  componentWillMount() {
    this.props.getTotalAmountOfUsers()
  }
  
  render() {
    return <Dashboard {...this.props}/>
  }
}

const mapStateToProps = ({dashboard, global}) => ({
  usersData: dashboard.usersData,
  endOfUsersList: dashboard.endOfUsersList,
  page: dashboard.page,
  usersAmount: dashboard.usersAmount,
  currentUserId: global.userLoggedId
})

const mapDispatchToProps = dispatch => bindActionCreators({getUsers: getUsersList, getTotalAmountOfUsers}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(DashboardContainer)