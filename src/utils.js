import {usersList} from './constants/users'

const getCookie = name => {
  var matches = document.cookie.match(new RegExp(
    // eslint-disable-next-line
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

const setCookie = (name, value, options) => {
  options = options || {};

  var expires = options.expires;

  if (typeof expires === "number" && expires) {
    var d = new Date();
    d.setTime(d.getTime() + expires * 1000);
    expires = options.expires = d;
  }
  if (expires && expires.toUTCString) {
    options.expires = expires.toUTCString();
  }

  value = encodeURIComponent(value);

  var updatedCookie = name + "=" + value;

  for (var propName in options) {
    updatedCookie += "; " + propName;
    var propValue = options[propName];
    if (propValue !== true) {
      updatedCookie += "=" + propValue;
    }
  }

  document.cookie = updatedCookie;
}

export const getUserId = () => {
  return getCookie('userId')
}

export const isCorrectLoginAndPassword = (email, password) => {
  return usersList.find(elem => elem.email === email && elem.password === password)
}

export const setUserId = (userId = '') => {
  setCookie('userId', userId)
}

export const getUserById = (userId = null) => {
  if (!userId)
    return false

  return usersList.find(elem => +elem.id === +userId)
}

export const getUsers = (page, count) => {
  const {from, to} = calcPaginationElemsFrom(page, count)
  return usersList.slice(from, to)
}

const calcPaginationElemsFrom = (page, count) => {
  let from = page
  let to = count
  if (page !== 0) {
    from = page * count
    to = from + count
  }
  return {from, to}
}

export const isEmailExist = (email) => {
  if (!usersList.find(item => item.email === email))
    return false

  return true
}

export const getAmountOfUsers = () => {
  return usersList.length
}

export const parseDate = date => {
  const msgDate = new Date(date),
    nowDate = new Date(Date.now()),
    msgDateWithouTime = (new Date(date)).setHours(0, 0, 0, 0),
    nowDateWithouTime = (new Date(nowDate.getTime())).setHours(0, 0, 0, 0),
    month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]

  if (msgDateWithouTime === nowDateWithouTime)
    return msgDate.getHours() + ":" + msgDate.getMinutes()

  if (msgDate.getYear() === nowDate.getYear())
    return month[msgDate.getMonth()] + " " + msgDate.getDate() + " " + msgDate.getHours() + ":" + msgDate.getMinutes()

  return msgDate.getFullYear() + " " + month[msgDate.getMonth()] + " " + msgDate.getDate() + " " + msgDate.getHours() + ":" + msgDate.getMinutes()
}